#include "shader.h"
#include <stdio.h>
#include <stdlib.h>

void create_shader(unsigned int *shader, char* vertexPath, char* fragmentPath) {
    FILE* vertexFile = fopen(vertexPath, "rb");

    fseek(vertexFile, 0, SEEK_END);
    size_t size = ftell(vertexFile);
    fseek(vertexFile, 0, SEEK_SET);

    char* vertexSource = malloc(size+1);
    fread(vertexSource, size, 1, vertexFile);
    fclose(vertexFile);
    vertexSource[size] = 0;

    FILE* fragmentFile = fopen(fragmentPath, "rb");

    fseek(fragmentFile, 0, SEEK_END);
    size = ftell(fragmentFile);
    fseek(fragmentFile, 0, SEEK_SET);

    char* fragmentSource = malloc(size+1);
    fread(fragmentSource, size, 1, fragmentFile);
    fclose(fragmentFile);
    fragmentSource[size] = 0;

    const char* vShaderSource = vertexSource;
    const char* fShaderSource = fragmentSource;


    unsigned int vertex, fragment;
    int success;
    char infoLog[512];

    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vShaderSource, NULL);
    glCompileShader(vertex);
    glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(vertex, 512, NULL, infoLog);
        printf("ERROR::SHADER::VERTEX::COMPILATION_FAILED\n %s\n", infoLog);;
    }

    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fShaderSource, NULL);
    glCompileShader(fragment);
    glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(fragment, 512, NULL, infoLog);
        printf("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n %s\n", infoLog);;
    }

    *shader = glCreateProgram();
    glAttachShader(*shader, vertex);
    glAttachShader(*shader, fragment);
    glLinkProgram(*shader);
    glGetProgramiv(*shader, GL_LINK_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(*shader, 512, NULL, infoLog);
        printf("ERROR::SHADER::PROGRAM::LINKING_FAILED\n %s\n", infoLog);;
    }

    glDeleteShader(vertex);
    glDeleteShader(fragment);
}

void use_shader(unsigned int shader) {
    glUseProgram(shader);
}

void set_bool(unsigned int shader, const char *name, bool value) {
    glUniform1i(glGetUniformLocation(shader, name), (int)value);
}

void set_int(unsigned int shader, const char *name, int value) {
    glUniform1i(glGetUniformLocation(shader, name), value);
}

void set_float(unsigned int shader, const char *name, float value) {
    glUniform1i(glGetUniformLocation(shader, name), value);
}

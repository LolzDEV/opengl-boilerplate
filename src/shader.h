#ifndef SHADER_H_
#define SHADER_H_

#include <GL/glew.h>
#include <stdbool.h>

void create_shader(unsigned int* shader, char* vertexPath, char* fragmentPath);
void use_shader(unsigned int shader);
void set_bool(unsigned int shader, const char* name, bool value);
void set_int(unsigned int shader, const char* name, int value);
void set_float(unsigned int shader, const char* name, float value);

#endif // SHADER_H_

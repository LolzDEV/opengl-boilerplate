CC = gcc
CFLAGS = -Wall -std=c11  
LIBS = -lm -lGL -lGLEW -lglfw -lcglm
SOURCES = $(wildcard src/*.c)
PROJECT_NAME = opengl_template
BUILD_DIR = build

all:  
	$(CC) $(CFLAGS) -o $(BUILD_DIR)/$(PROJECT_NAME) $(SOURCES) $(LIBS) -I. 

